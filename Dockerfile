ARG DEBIAN_VERSION
FROM debian:${DEBIAN_VERSION}
LABEL maintainer="Daniel von Essen"

ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies.
RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends \
  sudo systemd python3 \
  && rm -rf /var/lib/apt/lists/* \
  && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
  && apt-get clean

# Remove unnecessary getty and udev targets that result in high CPU usage when using
# multiple containers with Molecule (https://github.com/ansible/molecule/issues/1104)
RUN rm -f /lib/systemd/system/systemd*udev* \
  && rm -f /lib/systemd/system/getty.target

# Create `ansible` user with sudo permissions
RUN set -xe \
  && groupadd -r ansible \
  && useradd -m -g ansible ansible \
  && echo "%ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible

VOLUME ["/sys/fs/cgroup"]
CMD ["/lib/systemd/systemd"]
